set SHA=$(git rev-parse HEAD)
set CLOUDSDK_CORE_DISABLE_PROMPTS=1 # default y/N
echo $SHA

docker build -t gcr.io/dkr-k8-guide/multi-client:latest -f ./client/Dockerfile ./client
docker build -t gcr.io/dkr-k8-guide/multi-client:$SHA   -f ./client/Dockerfile ./client

docker build -t gcr.io/dkr-k8-guide/multi-server:latest -f ./server/Dockerfile ./server
docker build -t gcr.io/dkr-k8-guide/multi-server:$SHA   -f ./server/Dockerfile ./server
 
docker build -t gcr.io/dkr-k8-guide/multi-worker:latest -f ./worker/Dockerfile ./worker
docker build -t gcr.io/dkr-k8-guide/multi-worker:$SHA   -f ./worker/Dockerfile ./worker

docker push gcr.io/dkr-k8-guide/multi-client:latest
docker push gcr.io/dkr-k8-guide/multi-client:$SHA

docker push gcr.io/dkr-k8-guide/multi-server:latest
docker push gcr.io/dkr-k8-guide/multi-server:$SHA

docker push gcr.io/dkr-k8-guide/multi-worker:latest
docker push gcr.io/dkr-k8-guide/multi-worker:$SHA

kubectl apply -f k8s

kubectl set image deployments/client-deployment client=gcr.io/dkr-k8-guide/multi-client:$SHA
kubectl set image deployments/server-deployment server=gcr.io/dkr-k8-guide/multi-server:$SHA
kubectl set image deployments/worker-deployment worker=gcr.io/dkr-k8-guide/multi-worker:$SHA