# Section 16 - K8s Production Deployment

[GCP Cost calculator](https://cloud.google.com/products/calculator/)

Steps
- Link GCP project to billing if required
- Install Goole Coud SDK if not installed already
- ```gcloud components <update or install> kubectl```
- gcloud compute zone???
- gcloud container clusters get-credentials multi-cluster (cluster-2)
- Use $GIT_SHA as unique identifier

## Commands of note

Feor swithing between gcloud and local minikube
```bash
gcloud container clusters get-credentials cluster-2 --zone australia-southeast1-a --project dkr-k8-guide
kubectl config get-contexts
kubectl config use-context minikube
```

## Helm v3
Noteably for inside the GKE cluster / project.

[Install v3 from script](https://helm.sh/docs/intro/install/#from-script)

Then:
```bash
helm repo add stable https://kubernetes-charts.storage.googleapis.com/
helm install my-nginx stable/nginx-ingress --set rbac.create=true 
```

**(Helm 3) Skip the commands run in the following lectures: **
Helm Setup, Kubernetes Security with RBAC, Assigning Tiller a Service Account and Ingress-Nginx with Helm. You should still watch these lectures and they contain otherwise useful info. 

# Section 17 - HTTPS
- Buy domain
- Set up DNS
- Run these commands in the CloudShell

```bash
kubectl apply --validate=false -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.11/deploy/manifests/00-crds.yaml

```

- Add issuer.yaml
- Add certificate.yaml
- Apply config to generate certs
- Use ```kubectl get certificates``` to verify and ```kubectl describe certificates``` to get more detail  
- Modify ingress config to use certificates and tls
NOTE: Updating the rules in the ingress is a way to admin.domain.com (+dns changes)
- Apply config to use certs

# Section 18 - Local dev with Skaffold
- Install [skaffold](https://skaffold.dev/)
- Set up ```skaffold.yaml``` in project root

Note: To use skaffold for local dev, especially if a ```minikube delete``` has been performed, you will need to:
- Make sure ingress is enabled for minikube : ```minikube addons enable ingress```
- Have a dev version (no Helm required for certs-manager) of the ingress-service config.

# Google Container Registry (GCR)
So this section will be about me setting up and using GCR for the images instead of stephengrider/multi-xxxxx.

## Set up
As a general guide use [this](https://cloud.google.com/container-registry/docs/quickstart)

Sumnmary:
- gcloud installed, logged in and pointing to current project
- Config docker to use gcloud to authenticate requests to gcr: ```gcloud auth configure-docker```

Then use hello-world container as a test:
```log
‹gcr›$ docker tag hello-world gcr.io/dkr-k8-guide/hi:some-tag
‹gcr›$ docker push gcr.io/dkr-k8-guide/hi:some-tag
The push refers to repository [gcr.io/dkr-k8-guide/hi]
af0b15c8625b: Pushed 
some-tag: digest: sha256:92c7f9c92844bbbb5d0a101b22f7c2a7949e40f8ea90c8b3bc396879d95e899a size: 524
‹gcr›$ 
```

```log
‹gcr*›$ docker pull gcr.io/dkr-k8-guide/hi:some-tag
some-tag: Pulling from dkr-k8-guide/hi
Digest: sha256:92c7f9c92844bbbb5d0a101b22f7c2a7949e40f8ea90c8b3bc396879d95e899a
Status: Image is up to date for gcr.io/dkr-k8-guide/hi:some-tag
gcr.io/dkr-k8-guide/hi:some-tag
```

# PG dependency
One thing I noticed in the K8s section of the cource was that the connection to Postgres seemed flakey. Often connections would fail but occasionally they would.

I had something similar in docker-compose called wait-for-it and suspect this issue is that the pg-deployment takes longer to start up than the server/api deployment.

The K8s equivalent is the [initContainers](https://kubernetes.io/docs/concepts/workloads/pods/init-containers/) declaration

So I added this to the sever-deployment spec:
```yaml
initContainers:
  - name: init-wait
    image: postgres
    command: ['sh', '-c', 
      'until pg_isready -h pg-cluster-ip-service -p 5432; 
      do echo waiting for database; sleep 2; done;']
```

## References
https://www.postgresql.org/docs/9.3/app-pg-isready.html  
https://paquier.xyz/postgresql-2/postgres-9-3-feature-highlight-server-monitoring-with-pg_isready/  
https://kubernetes.io/docs/concepts/workloads/pods/init-containers/  
https://medium.com/@xcoulon/initializing-containers-in-order-with-kubernetes-18173b9cc222  
https://dzone.com/articles/kubernetes-demystified-solving-service-dependencie  
https://stackoverflow.com/questions/50838141/how-can-we-create-service-dependencies-using-kubernetes  

# Deployment take #2 
So I had to shut down my previous cluster as it was getting too expensive to run ($10 AUD per day ~) so this will be a good exercise to see if I remember what to do.

## Manual
From recollection it's kinda like this:
- set up the cluster in GKE
- connect to cluster from cloud shell
  - upgrade helm to v3
  - Add pg-secret
  - deplpoy nginx-ingress chart
  - deploy k8s/prod1
  - deploy cert manager / lets encrypt
  - deploy k8s/prod2

NB: Make sure the domain dns is point to right IP

Deployment #1 was:
- N2 x2 nodes (2x vCPU-casscade-lake 8gb RAM)
- ~$10 AUD per day

Deployment #2 was:
- N1 x2 nodes (1x vCPU-skylake 3.75Gb RAM)
- ~$?? AUD per day XXX not enough CPU ms resources

Deployment #3 was:
- N1-hicpu-2 x2 nodes (2x vCPU-skylake 1.8Gb RAM)
- ~$8 AUD per day 

Installing Helm-V3
```bash
curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 > get_helm.sh
chmod 700 get_helm.sh
./get_helm.sh
helm version
```

Adding PG secret
```bash
kubectl create secret generic pg-password --from-literal PGPASSWORD=postgres_password
```

Install Ingress-nginx
```bash
helm repo add stable https://kubernetes-charts.storage.googleapis.com/
helm install my-nginx stable/nginx-ingress --set rbac.create=true 
```

Set up cert manager / SSL
```bash
kubectl apply --validate=false -f https://github.com/jetstack/cert-manager/releases/download/v0.14.2/cert-manager.crds.yaml

kubectl create namespace cert-manager

helm repo add jetstack https://charts.jetstack.io

helm repo update

helm install \
  cert-manager jetstack/cert-manager \
  --namespace cert-manager \
  --version v0.14.2
```

Connecting to Cluster:
```bash
gcloud container clusters get-credentials dkr-k8-guide --zone australia-southeast1-a --project dkr-k8-guide
```

## References
https://charlescary.com/bringing-on-prem-kubernetes-to-cloud-parity/  
https://www.zdnet.com/article/what-is-the-kubernetes-hybrid-cloud-and-why-it-matters/  

https://platform9.com/blog/kubernetes-on-premises-why-and-how/  
https://www.ovhcloud.com/en-au/public-cloud/kubernetes/ 

https://www.datacenterknowledge.com/business/after-kubernetes-victory-its-former-rivals-change-tack  
https://www.datacenterknowledge.com/open-source/explaining-knative-project-liberate-serverless-cloud-giants  
https://brianchristner.io/what-is-docker-buildkit/  
https://www.docker.com/blog/advanced-dockerfiles-faster-builds-and-smaller-images-using-buildkit-and-multistage-build/  
https://www.mamboserver.com/digitalocean-alternatives/  
https://syd-au-ping.vultr.com/    
https://www.cloudways.com/en/vultr-hosting.php?id=488568  
https://www.linode.com/lp/free-credit/?utm_source=google&utm_medium=cpc&utm_campaign=6450592077_82778259568&utm_term=g_aud-832126558766:kwd-2629795801_e_linode&utm_content=427766173431&locationid=9071399&device=c_c&gclid=EAIaIQobChMI25GG_7SD6QIVWyQrCh3_qQdVEAAYASAAEgIKgvD_BwE  
https://www.ovhcloud.com/en-au/?xtor=SEC-13-GOO-[GG-LAB-AU-EN-SRC-DEF-BRAND(OVH|Exa)]-[427279323535]-S-[ovh]&xts=563736&sitelink=&gclid=EAIaIQobChMIzvjgqreD6QIVzCMrCh0y9QIyEAAYASAAEgIUAvD_BwE  

# Single Container Update - Manual

Steps:
- Update a client file (trivial mod)
- Commit and push (new git SHA)
- Build and push update client image
- Manually update k8 client deplyment image
- Refresh page to verify update